#!/bin/sh

DDNS_CMD=`python2.7 -c "import jinja2 ; print jinja2.Template(open('./profiles/certbot/certificates/$CERTBOT_IDENTITY/ddns-cmd.j2').read()).render(certbot__dns_record='_acme-challenge.$CERTBOT_DOMAIN', certbot__dns_value='$CERTBOT_VALIDATION')"`
echo "$DDNS_CMD" > "./profiles/certbot/certificates/$CERTBOT_IDENTITY/ddns-cmd"
nsupdate -k "./profiles/certbot/certificates/$CERTBOT_IDENTITY/ddns-key" "./profiles/certbot/certificates/$CERTBOT_IDENTITY/ddns-cmd" &> ./profiles/certbot/certificates/$CERTBOT_IDENTITY/nsupdate.result

sleep 60
