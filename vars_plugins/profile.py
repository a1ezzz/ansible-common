
import os
import re
from ansible.parsing.dataloader import DataLoader


class ProfileVars:
	
	def __init__(self):
		self.profile_vars = {}

	def vars(self):
		return self.profile_vars

	@staticmethod
	def deep_combine(initial_var, *combine_with):
	
		def combine(var_a, var_b):
			if var_a is None:
				return var_b
			elif var_b is None:
				return var_a
			elif isinstance(var_a, dict) is True:
				assert(isinstance(var_b, dict) is True)
				result = var_a.copy()
				for k in var_b.keys():
					result[k] = combine(var_a[k] if k in var_a else None, var_b[k])
				return result
			elif isinstance(var_a, list) is True:
				assert(isinstance(var_b, list) is True)
				result = []
				result.extend(var_a)
				for i in var_b:
					result.append(i)
				return result
			else:
				return var_b
			
		result = initial_var
		for v in combine_with:
			result = combine(result, v)
	
		return result


class RoleProfile(ProfileVars):

	identity_re = re.compile('^(.*)\.(yml|yaml)$')

	def __init__(self, role_directory, dataloader):
		ProfileVars.__init__(self)
		self.role_directory = role_directory

		for entry in os.listdir(self.role_directory):
			entry_path = os.path.join(self.role_directory, entry)
			if os.path.isfile(entry_path) is False:
				continue
			re_match = RoleProfile.identity_re.match(entry)
			identity = re_match.group(1) if re_match is not None else entry
			self.profile_vars[identity] = dataloader.load_from_file(entry_path)
	
	def role_profile(self, *identities):
		result = {}
		for i in identities:
			identity_profile = self.profile_vars[i] if i in self.profile_vars else {}
			result = self.deep_combine(result, identity_profile)

		return result


class ProfileStorage:

	__profile_main_directory__ = './profiles'

	def __init__(self, vault_password=None):
		self.roles = {}
		self.dataloader = DataLoader()
		if vault_password is not None:
			self.dataloader.set_vault_password(vault_password)
		
		if os.path.isdir(self.profile_directory()) is False:
			return
		
		for entry in os.listdir(self.profile_directory()):
			entry_path = os.path.join(self.profile_directory(), entry)
			if os.path.isdir(entry_path) is True:
				self.roles[entry] = RoleProfile(entry_path, self.dataloader)

	def all_profiles(self):
		result = {}
		for role_name, role_profile in self.roles.items():
			result[role_name] = role_profile.vars()
		return result
	
	def entry_profile(self, *identities):
		result = {}
		for role_name, role_profile in self.roles.items():
			result[role_name] = role_profile.role_profile('defaults', *identities)
		return result

	@classmethod
	def profile_directory(cls):
		return cls.__profile_main_directory__

	@classmethod
	def identities(cls, host, inventory):
		result = []
		
		# generate group identities
		group_identities = {}
		for group in host.get_groups():
			identity = str(group.vars['profile_name']) if 'profile_name' in group.vars else group.name
			group_identities[group.name] = identity
		
		# append groups identities
		group_names = list(group_identities)
		group_names.sort()
		for name in group_names:
			result.append(group_identities[name])

		# append host identity
		host_vars = host.get_vars()
		result.append(str(host_vars['profile_name']) if 'profile_name' in host_vars else host.name)
		return tuple(result)


class VarsModule:

	def __init__(self, inventory):
		self.inventory = inventory
		
	def run(self, host, vault_password=None):
		return self.get_host_vars(host, vault_password=vault_password)
		
	def get_host_vars(self, host, vault_password=None):
		if vault_password is None:
				if hasattr(self.inventory._loader, '_vault_password') is True:
					if self.inventory._loader._vault_password is not None:
						vault_password = self.inventory._loader._vault_password
				if hasattr(self.inventory._loader, '_b_vault_password') is True:
					if self.inventory._loader._b_vault_password is not None:
						vault_password = self.inventory._loader._b_vault_password

		profiles = ProfileStorage(vault_password=vault_password)

		host_vars = host.get_vars()

		identities = profiles.identities(host, self.inventory)
		return {
			'all_profiles': profiles.all_profiles(),
			'host_profile': profiles.entry_profile(*identities),
			'profile_name': host_vars['profile_name'] if 'profile_name' in host_vars else host.name
		}
		
	def get_group_vars(self, group, vault_password=None):
		return {}

