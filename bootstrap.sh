#!/bin/bash

USAGE="Please, enter the hostname or ip and target server name.
Like \"$0 <host.name> <profile-name>\"
just like ./bootstrap.sh 10.254.254.247 dns-ntp
"

if [ -z $1 ] || [ -z $2 ]
then
	echo $USAGE
	exit -1
fi

if [ -z $BECOME_METHOD ]
	then
		BECOME_METHOD="su"
fi

if [ -z $ANSIBLE_USER ]
	then
		ANSIBLE_USER="administrator"
fi

if [ -z $PLAYBOOK ]
	then
		PLAYBOOK="bootstrap.yml"
fi

HOST=$1
PROFILE_NAME=$2

echo "Bootstraping host \"$HOST\" with playbook $PLAYBOOK (remote user is $ANSIBLE_USER)"

IS_KNOWN=`grep -oE "^$HOST[\, ].*" ~/.ssh/known_hosts`
if [ "$IS_KNOWN" == "" ]
then
	echo "Set host $HOST as known to ~/.ssh/known_hosts"
	ssh-keyscan -t ecdsa $HOST >> ~/.ssh/known_hosts
fi

ANSIBLE_INVENTORY=`tempfile -p ansible -d .`
echo "(Ansible inventory is at $ANSIBLE_INVENTORY)"
echo > $ANSIBLE_INVENTORY
echo "$HOST ansible_user=$ANSIBLE_USER profile_name='$PROFILE_NAME'" >> $ANSIBLE_INVENTORY 
echo "[group__bootstrap]" >> $ANSIBLE_INVENTORY 
echo "$HOST" >> $ANSIBLE_INVENTORY 
echo >> $ANSIBLE_INVENTORY

ANSIBLE_INVENTORY="$ANSIBLE_INVENTORY" ansible-playbook $PLAYBOOK --become-method="$BECOME_METHOD" -v --ask-pass --ask-become-pass --extra-vars "bootstrap_host=$HOST"

rm $ANSIBLE_INVENTORY
